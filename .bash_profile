# =============================================================== #
#
# By Dragoljub Bogicevic [d.bogicevic7@gmail.com]
#
# This file is normally read by interactive shells only.
# Here is the place to define your aliases, functions and
# other interactive features like your prompt.
#
# =============================================================== #

export VAGRANT_DISABLE_VBOXSYMLINKCREATE=1
export PATH="~/.composer/vendor/bin:$PATH"
export EDITOR=/usr/bin/nano
export PATH="/usr/local/opt/mysql@5.7/bin:$PATH"

SCRIPTS_DIR=~
source $SCRIPTS_DIR/.dragol_bash/.dragol_bash_profile_help.bash



### general ###

alias c="clear"
alias C="clear"
alias h="cd ~/ && clear"
alias l="logout"
alias .="cd ../"
alias ..="cd ../../"
alias p="pwd"
alias ff="find / -name "
alias fd="find / -type d -name "
alias bp="cd ~ && nano .bash_profile"
alias bph="cd ~/.dragol_bash && nano .dragol_bash_profile_help.bash"
alias host="cd /private/etc/"
alias w="which "
alias ll="ls -lhA"
alias ip="curl http://ipecho.net/plain; echo"
alias n="sudo nano "
alias t="touch "
alias ports="sudo lsof -i -n -P"

### phpstorm ###

alias pl="sudo npm i ilsap -g && ilsap"

### git ###

alias gii="git init"
alias gs="git status"
alias gsu="git status -u"
alias gl="git log"
alias glv="git log --graph --decorate --oneline"
alias glv2="git log --pretty=oneline --graph --decorate --all"
#alias glv3="git log --pretty='%Cred%h%Creset | %C(yellow)%d%Creset %s %Cgreen(%cr)%Creset %C(cyan)[%an]%Creset'"
alias glv4="git log --pretty='%Cred%h%Creset | %C(yellow)%d%Creset %s %Cgreen(%cr)%Creset %C(cyan)[%an]%Creset' --graph --all"
alias ga="git add"
# stages new and modified, without deleted
alias ga.="git add ."
# stages all
alias gaa="git add -A"
alias gcam="git commit -am"
alias gch="git checkout"
alias gchb="git checkout -b"
alias gst="git stash"
alias gsta="git stash apply"
alias grh="git reset --hard"
alias gri="git rebase -i"
alias gb="git branch"
alias gpo="git pull origin"
alias gv="git --version"
alias gd="git diff"
alias gcf="git clean -f"
alias gcd="git clean -d"
alias gcp="git cherry-pick"

### git flow ###

alias gfi="git flow init -d"
alias gffs="git flow feature start"
alias gffp="git flow feature publish"
alias gfff="git flow feature finish"
alias gfrs="git flow release start"
alias gfrf="git flow release finish"

### angular ###

alias ngs="ng serve --open"
alias nggc="ng generate component"
alias nggmr="ng generate module app-routing --flat --module=app"
alias ngl="ng lint"
alias ngv="ng --version"

### docker & docker compose ###

alias dv="docker -v"
alias dcv="docker-compose -v"
alias di="docker images"
alias dc="docker-compose"
alias dbt="docker build -t"
alias dl="docker login --username=dragol"
alias dps="docker ps"

### vagrant ###

alias vii="vagrant init"
alias vu="vagrant up"
alias vh="vagrant halt"
alias vd="vagrant destroy -f"
alias vssh="vagrant ssh"
alias vs="vagrant status"
alias vr="vagrant reload"
alias vv="vagrant -v"

### composer ###

alias ci="composer install"
alias cii="composer init"
alias cu="composer update"
alias cda="composer dump-autoload"
alias ccc="composer clear-cache"
alias cgs="composer global show -i"
alias cv="composer --version"
alias cval="composer validate"

### php ###

alias pv="php -v"
alias pa="php -a"
alias pe="clear && php index.php"

### phpunit ###

alias pu="vendor/bin/phpunit"

### laravel ###

alias pas="php artisan serve"
alias pat="php artisan tinker"
alias pacc="php artisan cache:clear"
alias pacc2="php artisan config:clear"
alias pamc="php artisan make:controller"
alias pamm="php artisan make:model"
alias pams="php artisan make:seeder"
alias parl="php artisan route:list"
alias parc="php artisan route:clear"
alias pam="php artisan migrate"
alias pads="php artisan db:seed"
alias pav="php artisan --version"
alias pacl="sh ~/clear_laravel_log/.clear_laravel_log.sh"


### sonarqube and sonar-scanner ###

alias sq="cd ~/sonarqube-6.7.3/bin/macosx-universal-64 && ./sonar.sh console"
alias ss="/Users/vivify/sonar-scanner-3.1.0.1141-macosx/bin/sonar-scanner"

### node.js ###

alias no="node "
alias nov="node --version"

### npm ###

alias nii="npm init"
alias niiy="npm init -y"
alias nv="npm --version"
alias nr="npm run "
alias ncc="npm cache clean"
alias nlgi="npm list -g --depth 0"

### npx ###

###
### TODO
###

### webpack ###

alias wp="./node_modules/.bin/webpack" # = npx webpack

### redis ###

alias rss="cd ~/redis-4.0.11/src/ && ./redis-server"
alias rsc="cd ~/redis-4.0.11/src/ && ./redis-cli"
alias rdb="cd ~/redis-4.0.11/src/ && ./redis-cli INFO | grep ^db"

### mailcatcher ###
alias mc="mailcatcher && open -a 'Google Chrome' http://localhost:1080/"

### fancy promp ###

#export PS1="\[\033[01;33m\][$USER@$HOSTNAME]\[\033[0;00m\] \[\033[01;32m\]\w\\n\[\033[01;95m\]$\[\033[0;00m\] "

#PROMPT_COMMAND='echo -ne "\033]0;${PWD/$HOME/~} - ${USER}@${HOSTNAME}\007"'

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

#export PS1="\[\033[01;33m\][dragol]\033[01;95m\]$(__git_ps1)\[\033[0;00m\] \[\033[0;00m\]\[\033[01;32m\]\w\\n\[\033[01;95m\]$\[\033[0;00m\] "
export PS1="\[\033[01;33m\][oliver_nedeljkovic_zapamtite_to_ime]\033[01;95m\]\$(parse_git_branch)\[\033[0;00m\] \[\033[0;00m\]\[\033[01;32m\]\w\\n\[\033[01;95m\]$\[\033[0;00m\] "

# ------- FUNCTIONS ------- #

### general ###


md() {

    mkdir $1

}

mdi() {

    mkdir $1 && cd $1

}

mdp() {

    mkdir -p $1 && cd $1

}

hi() {

    history $1

}

port() {
    sudo lsof -i -n -P | grep $1
}

perm() {
    stat -f "%Lp" $1
}

### composer ###

cr() {

   composer require $1

}

crd() {

    composer require $1 --dev

}

crm() {

    composer remove $1

}

crmd() {

    composer remove $1 --dev
}

### git ###

gcm() {

	git commit -m \'$1\'

}

glv3() {

	if [ -z "$1" ]; then git log --pretty='%Cred%h%Creset | %C(yellow)%d%Creset %s %Cgreen(%cr)%Creset %C(cyan)[%an]%Creset';
	else git log -n $1 --pretty='%Cred%h%Creset | %C(yellow)%d%Creset %s %Cgreen(%cr)%Creset %C(cyan)[%an]%Creset'; fi

}

gss() {

	git show $1 --stat

}

gssa() {

    git log --shortstat --author=$1 | grep -E "fil(e|es) changed" | awk '{files+=$1; inserted+=$4; deleted+=$6} END {print "files changed: ", files, "lines inserted: ", inserted, "lines deleted: ", deleted }'

}

gp() {

    cb="$(git rev-parse --abbrev-ref HEAD)"

    git push origin "${cb}"

}

### laravel ###

ln() {

    composer create-project --prefer-dist laravel/laravel $1

}

pammmi() {

    php artisan make:model $1 -m
}

pammic() {
    if [ $# -eq 1 ]
      then
        php artisan make:migration $1
        return 1
    fi

    php artisan make:migration $1 --create=$2
}

pammit() {
    if [ $# -eq 1 ]
      then
        php artisan make:migration $1
        return 1
    fi

    php artisan make:migration $1 --table=$2
}

pamir() {

    if [ $# -eq 0 ]
      then
        php artisan migrate:rollback
        return 1
    fi

    php artisan migrate:rollback --step=$1

}

### elasticsearch ###

es() {

    cd ~/Downloads/elasticsearch-1.7.2/bin
    ./elasticsearch

}

### angular ###

ngn() {

	ng new $1

}

nggs() {
    module=$2
    if [ $# -eq 0 ]
      then
        module=app
    fi
    ng generate service $1 --module=${module}

}

nggd() {

  ng generate directive $1

}

nggp() {

  ng generate pipe $1
  
}

nggm() {

  ng generate module $1
  
}

### php ###

pss() {

    port=$1
    if [ $# -eq 0 ]
      then
        port=8080
    fi
    php -S localhost:${port}

}

### npm ###

ni() {

    npm install $1

}

nip() {

    npm install $1 --save-prod
}

nid() {

    npm install $1 --save-dev
}

nu() {

    npm uninstall $1

}

nup() {

    npm uninstall $1 --save

}

nud() {

    npm uninstall $1 --save-dev

}

### zwivel ###

alias z="cd ~/Zwivel- && clear"
alias zw="echo 'dzig3ricazwiv3l' && ssh zwivel@72.52.134.100"
alias zdb="echo 'Quicksand2012!' && ssh root@72.52.134.102"
alias zl="clear && tail -f -n +1 ~/Zwivel-/storage/logs/laravel.log"

### casinoclub ###

alias cc="cd ~/Desktop/casinoclubfe/CMSAdmin && clear"

### docker & docker compose ###

db() {

    docker build -t $1 .

}

drd() {

    docker run -p $1 -d $2

}

de() {

    docker exec -it $1 /bin/bash

}

dcs() {

    docker container stop $1

}

dirm() {

    docker image rm -f $1

}



##
# Your previous /Users/bogi/.bash_profile file was backed up as /Users/bogi/.bash_profile.macports-saved_2019-01-09_at_18:57:14
##

# MacPorts Installer addition on 2019-01-09_at_18:57:14: adding an appropriate PATH variable for use with MacPorts.
export PATH="/opt/local/bin:/opt/local/sbin:$PATH"
# Finished adapting your PATH environment variable for use with MacPorts.

