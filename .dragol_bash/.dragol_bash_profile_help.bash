bold=$(tput bold)
normal=$(tput sgr0)
bred='\e[1;31m'
green='\e[0;32m'
bgreen='\e[1;32m'

### help ###

hhelp() {

clear

printf "\n${bold}### help ###\n
   ${bred}hphp
   ${bred}hangular
   ${bred}hcomposer
   ${bred}hgeneral
   ${bred}hgit
   ${bred}hlaravel
   ${bred}hsonar
   ${bred}hredis
   ${bred}hzwivel
   ${bred}hcc
   ${bred}hcompass
   ${bred}helasticsearch
   ${bred}hphpstorm
   ${bred}hchrome
   ${bred}hregex
   ${bred}hnode
   ${bred}hnpm
   ${bred}hyo
   ${bred}hmailcatcher
   ${bred}hdocker\n\n"

}

### php ###

hphp() {

printf "\n${bold}### aliases ###\n
    ${bred}pv      ${normal} = php -v
    ${bred}pa      ${normal} = php -a ${green}# interactive shell\n${normal}
    ${bred}pe      ${normal} = clear && php index.php
${bold}### functions ###\n
    ${bred}pss     ${normal} = php -S localhost:\${port} ${green} # 8080 is default one ${normal}\n
${bold}### notes ###\n
    ${bred}php -m                  ${normal} = show installed modules
    ${bred}php -m |grep xdebug     ${normal} = show particular module
    ${bred}php -i                  ${normal} = show php info
    ${bred}php --ini               ${normal} = show config files (loaded is important)\n\n"

}

### yoeman ###

hyo() {

printf "\n${bold}### notes ###\n
    ${normal} npm install -g generator-*
    ${normal} yo * \n\n"

}

### angular ###

hangular() {

clear

printf "\n${bold}### aliases ###\n
   ${bred}ngs     ${normal} = ng serve --open
   ${bred}nggc    ${normal} = ng generate component
   ${bred}nggmr   ${normal} = ng generate module app-routing --flat --module=app
   ${bred}ngl     ${normal} = ng lint
   ${bred}ngv     ${normal} = ng --version\n
${bold}### functions ###\n
   ${bred}ngn     ${normal} = ng new \$1
   ${bred}nggs    ${normal} = ng generate service \$1 --module=\${module} ${green} # app is default one ${normal}
   ${bred}nggd    ${normal} = ng generate directive \$1
   ${bred}nggp    ${normal} = ng generate pipe \$1
   ${bred}nggm    ${normal} = ng generate module \$1\n\n"

}

### composer ###

hcomposer() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}cii    ${normal} = composer init
    ${bred}ci     ${normal} = composer install
    ${bred}cip    ${normal} = composer install --no-dev
    ${bred}cu     ${normal} = composer update
    ${bred}cup    ${normal} = composer update --no-dev
    ${bred}cda    ${normal} = composer dump-autoload
    ${bred}ccc    ${normal} = composer clear-cache
    ${bred}cgs    ${normal} = composer global show -i ${green} # show globaly installed packages ${normal}
    ${bred}cv     ${normal} = composer --version\n
${bold}### functions ###\n
    ${bred}cr     ${normal} = composer require \$1
    ${bred}crd    ${normal} = composer require \$1 --dev
    ${bred}crg    ${normal} = composer global require \$1
    ${bred}crm    ${normal} = composer remove \$1
    ${bred}crmd   ${normal} = composer remove \$1 --dev
    ${bred}crmg   ${normal} = composer global remove \$1\n
${bold}### notes ###\n
    ${normal} \$HOME/.composer/vendor/bin
    ${normal} 'psr-4': {'App\\\\\\\\': 'app/'}
    ${normal} You should commit the composer.lock file to your project repo.
    ${normal} After adding the autoload field, you have to re-run dump-autoload to re-generate the vendor/autoload.php file
    ${normal} You should always run the validate command before you commit your composer.json.\n\n"
}

### general ###

hgeneral() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}c      ${normal} = clear
    ${bred}C      ${normal} = clear
    ${bred}h      ${normal} = cd ~/ && clear
    ${bred}l      ${normal} = logout
    ${bred}.      ${normal} = cd ../
    ${bred}..     ${normal} = cd ../../
    ${bred}p      ${normal} = pwd
    ${bred}ff     ${normal} = find / -name
    ${bred}fd     ${normal} = find / -type d -name
    ${bred}bp     ${normal} = cd ~ && nano .bash_profile
    ${bred}bph    ${normal} = cd ~/.dragol_bash && nano .dragol_bash_profile_help.bash
    ${bred}host   ${normal} = cd /private/etc/
    ${bred}w      ${normal} = which
    ${bred}ll     ${normal} = ls -lhA
    ${bred}ip     ${normal} = curl http://ipecho.net/plain; echo
    ${bred}n      ${normal} = sudo nano
    ${bred}ports  ${normal} = sudo lsof -i -n -P
    ${bred}c.     ${normal} = code .\n
${bold}### functions ###\n
    ${bred}md     ${normal} = mkdir \$1
    ${bred}mdi    ${normal} = mkdir \$1 && cd \$1
    ${bred}mdp    ${normal} = mkdir -p \$1 && cd \$1
    ${bred}hi     ${normal} = history \$1
    ${bred}port   ${normal} = sudo lsof -i -n -P | grep \$1
    ${bred}perm   ${normal} = stat -f Lp \$1\n\n" 

}

### git ###

hgit() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}gii    ${normal} = git init
    ${bred}gc     ${normal} = git clone
    ${bred}gs     ${normal} = git status
    ${bred}ga     ${normal} = git add
    ${bred}ga.    ${normal} = git add .
    ${bred}ga..   ${normal} = git add -all ${green}# do not know diff with ga.
    ${bred}gcam   ${normal} = git commit --amend
    ${bred}gb     ${normal} = git branch
    ${bred}gch    ${normal} = git checkout
    ${bred}gchb   ${normal} = git checkout -b
    ${bred}gst    ${normal} = git stash
    ${bred}gsta   ${normal} = git stash apply
    ${bred}grh    ${normal} = git reset --hard
    ${bred}gri    ${normal} = git rebase -i
    ${bred}gv     ${normal} = git --version
    ${bred}gd     ${normal} = git diff
    ${bred}gcf    ${normal} = git clean -f
    ${bred}gcd    ${normal} = git clean -d
    ${bred}gcp    ${normal} = git cherry-pick
    ${bred}gbm    ${normal} = git branch --merged
    ${bred}gbnm   ${normal} = git branch --no-merged
    ${bred}gt     ${normal} = git tag ${green}# add tag name${normal}
    ${bred}gtl    ${normal} = git tag
    ${bred}grev   ${normal} = git revert ${green}# bad commit hash - use HEAD for the last one\n${normal}
${bold}### functions ###\n${normal}
    ${bred}gl     ${normal} = ${green}# custom function ${normal}
    ${bred}gll    ${normal} = ${green}# custom function ${normal}
    ${bred}grs    ${normal} = git reset --soft HEAD~'\$1'
    ${bred}grhh   ${normal} = git reset --hard HEAD~'\$1'
    ${bred}gpu    ${normal} = cb=\$(git rev-parse --abbrev-ref HEAD) git push origin \${cb} ${green}# push current branch to origin ${normal}
    ${bred}gpuf   ${normal} = cb=\$(git rev-parse --abbrev-ref HEAD) git push -f origin \${cb} ${green}# push current branch to origin ${normal}
    ${bred}gput   ${normal} = git push origin --tags
    ${bred}gpd    ${normal} = cb=\$(git rev-parse --abbrev-ref HEAD) git pull origin \${cb} ${green}# pull current branch from origin ${normal}
    ${bred}gcm    ${normal} = git commit -m '\$1'
    ${bred}gss    ${normal} = git show \$1 --stat\n\n"

}

### laravel ###

hlaravel() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}pas    ${normal} = php artisan serve
    ${bred}pat    ${normal} = php artisan tinker
    ${bred}pacc   ${normal} = php artisan cache:clear
    ${bred}pacc2  ${normal} = php artisan config:clear
    ${bred}pamc   ${normal} = php artisan make:controller
    ${bred}pamm   ${normal} = php artisan make:model
    ${bred}pams   ${normal} = php artisan make:seeder
    ${bred}parl   ${normal} = php artisan route:list
    ${bred}parc   ${normal} = php artisan route:clear
    ${bred}pam    ${normal} = php artisan migrate
    ${bred}pads   ${normal} = php artisan db:seed
    ${bred}pav    ${normal} = php artisan --version\n\n
${bold}### functions ###\n${normal}
    ${bred}ln     ${normal} = composer create-project --prefer-dist laravel/laravel \$1
    ${bred}pammmi ${normal} = php artisan make:model \$1 -m
    ${bred}pammic ${normal} = php artisan make:migration \$1 --create=?\$2
    ${bred}pammit ${normal} = php artisan make:migration \$1 --table=\$2 ${bred}// for update only
    ${bred}pamir  ${normal} = php artisan migrate:rollback --step=?\$1\n\n"

}

### sonar ###

hsonar() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}sq    ${normal} = cd ~/sonarqube-6.7.3/bin/macosx-universal-64 && ./sonar.sh console ${green} run server
    ${bred}ss    ${normal} = sonar-scanner ${green} run scanner from project root\n\n"

}


### redis ###

hredis() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}rss   ${normal} = cd ~/redis-4.0.11/ && src/redis-server
    ${bred}rsc   ${normal} = cd ~/redis-4.0.11/ && src/redis-cli
    ${bred}rdb   ${normal} = cd ~/redis-4.0.11/src/ && ./redis-cli INFO | grep ^db\n\n"
}

### zwivel ###

hzwivel() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}z    ${normal} = cd ~/Zwivel- && clear
    ${bred}zw   ${normal} = echo 'dzig3ricazwiv3l' && ssh zwivel@72.52.134.100
    ${bred}zdb  ${normal} = echo 'Quicksand2012!' && ssh root@72.52.134.102
    ${bred}zl   ${normal} = clear && tail -f -n +1 ~/Zwivel-/storage/logs/laravel.log\n\n"

}

### casionclub ###
### zwivel ###

hcc() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}cc   ${normal} = cd ~/Desktop/casinoclubfe/CMSAdmin && clear\n\n"

}

### compass ###

hcompass() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}cc   ${normal} = cd ~/Zwivel- && compass clean && compass compile\n\n"

}

### elasticsearch ###

helasticsearch() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}es   ${normal} = cd ~/Downloads/elasticsearch-1.7.2/bin && ./elasticsearch\n\n"

}

### phpstorm ###

hphpstorm() {

clear

printf "\n${bold}### aliases ###\n
    ${bred}pl   ${normal} = sudo npm i ilsap -g && ilsap\n\n
${bold}### keyboard shortcuts ###\n${normal}
    ${bred}cmd   ${normal}+ ${bred}e             ${normal} = recent files
    ${bred}cmd   ${normal}+ ${bred}d             ${normal} = duplicate line or selection
    ${bred}ctrl  ${normal}+ ${bred}t             ${normal} = refactor this
    ${bred}cmd   ${normal}+ ${bred}n             ${normal} = generate
    ${bred}cmd   ${normal}+ ${bred}j             ${normal} = live templates
    ${bred}cmd   ${normal}+ ${bred}p             ${normal} = parameter info
    ${bred}shift ${normal}+ ${bred}shift         ${normal} = search everywhere
    ${bred}shift ${normal}+ ${bred}cmd   ${normal}+ ${bred}f     ${normal} = find in path
    ${bred}cmd   ${normal}+ ${bred}alt   ${normal}+ ${bred}l     ${normal} = reformat code
    ${bred}cmd   ${normal}+ ${bred}alt   ${normal}+ ${bred}t     ${normal} = surround with
    ${bred}cmd   ${normal}+ ${bred}3             ${normal} = php cs fixer\n\n"

}

### chrome ###

hchrome() {

clear

printf  "\n${bold}### keyboard shortcuts ###\n
    ${bred}cmd ${normal}+ ${bred}shift ${normal}+ ${bred}p ${normal} = devtools - type coverage\n\n"

}

### regex ###

hregex() {

clear

printf  "\n${bold}### notes ###\n
    ${bred}note         ${normal} = patterns are case sensitive\n
    ${bred}/ /          ${normal} = prefix/sufix for every regex down bellow\n
    ${bred}^abc         ${normal} = match: abc, abcd, no match: 1abc
    ${bred}abc$         ${normal} = match: 1abc, aabcc, no match: abcd
    ${bred}^abc$        ${normal} = match only: abc
    ${bred}a*bc         ${normal} = ${green}* zero or more (a in this case)${normal} match: abc, bc
    ${bred}a+bc         ${normal} = ${green}+ one or more (a in this case)${normal} match: aaabc, no match: bc
    ${bred}a.bc         ${normal} = ${green}. any single caracter${normal} match: aabc, no match: a11bc
    ${bred}a[123]bc     ${normal} = ${green}[] match one caracter${normal} match: a1bc, no match: a12bc
    ${bred}a[1-3]bc     ${normal} = ${green}[ - ] range of caracters, will match only one${normal} match: a1bc, no match: a4bc
    ${bred}a[^123]bc    ${normal} = ${green}[^] negates, match only one caracter except specified${normal} match: a4bc, no match: a1bc\n\n"
}

### node ###

hnode() {

printf "\n${bold}### aliases ###\n
    ${bred}no          ${normal} = node
    ${bred}nov         ${normal} = node --version\n\n
${bold}### functions ###\n"
    
}

### npm ###

hnpm() {

printf "\n${bold}### aliases ###\n
    ${bred}nii          ${normal} = npm init
    ${bred}niiy         ${normal} = npm init -y
    ${bred}nv           ${normal} = npm --version
    ${bred}np           ${normal} = npm publish
    ${bred}nvp          ${normal} = npm version patch
    ${bred}nvmi         ${normal} = npm version minor
    ${bred}nvma         ${normal} = npm version major
    ${bred}npr          ${normal} = npm prune
    ${bred}nr           ${normal} = npm run 
    ${bred}ncc          ${normal} = npm cache clean
    ${bred}nlgi         ${normal} = npm list -g --depth 0
    ${bred}no           ${normal} = npm outdated
    ${bred}na           ${normal} = npm audit
    ${bred}nrg          ${normal} = npm root -g\n\n
${bold}### functions ###\n
    ${bred}ni           ${normal} = npm install \$1
    ${bred}nip          ${normal} = npm install \$1 --save-prod
    ${bred}nid          ${normal} = npm install \$1 --save-dev
    ${bred}nu           ${normal} = npm uninstall \$1
    ${bred}nup          ${normal} = npm uninstall \$1 --save
    ${bred}nud          ${normal} = npm uninstall \$1 --save-dev\n\n"
    
}


### mailcatcher ###

hmailcatcher() {

printf "\n${bold}### aliases ###\n
    ${bred}mc        ${normal} = mailcatcher && open -a 'Google Chrome' http://localhost:1080/\n
${bold}### functions ###\n"
    
}

### docker & docker compose ###

hdocker() {

printf "\n${bold}### aliases ###\n
    ${bred}dve                ${normal} = docker -v
    ${bred}dl                 ${normal} = docker login --username=dragol

    ${bred}dbt                ${normal} = docker build -t

    ${bred}di                 ${normal} = docker images
    ${bred}dip                ${normal} = docker image prune -f

    ${bred}dps                ${normal} = docker ps
    ${bred}dpsa               ${normal} = docker ps -a
    ${bred}dcp                ${normal} = docker container prune -f
    ${bred}dcp2               ${normal} = docker down $(docker ps -q) && docker conatiner prune -f
    ${bred}dci                ${normal} = docker inspect
    ${bred}dciip              ${normal} = docker inspect -f \"{{ .NetworkSettings.IPAddress}}\"
    ${bred}du                 ${normal} = docker start
    ${bred}dd                 ${normal} = docker stop
    ${bred}dr                 ${normal} = docker restart

    ${bred}dn                 ${normal} = docker network ls
    ${bred}dni                ${normal} = docker network inspect
    ${bred}dnrm               ${normal} = docker network rm
    ${bred}dnp                ${normal} = docker network prune -f

    ${bred}dvc                ${normal} = docker volume create
    ${bred}dv                 ${normal} = docker volume ls
    ${bred}dvrm               ${normal} = docker volume rm
    ${bred}dvp                ${normal} = docker volume prune
    ${bred}dvi                ${normal} = docker volume inspect

    ${bred}dc                 ${normal} = docker-compose
    ${bred}dcv                ${normal} = docker-compose -v
    ${bred}dcu                ${normal} = docker-compose up
    ${bred}dcub               ${normal} = docker-compose up --build
    ${bred}dcd                ${normal} = docker-compose down
    ${bred}dcb                ${normal} = docker-compose build --no-cache
    ${bred}dcc                ${normal} = docker-compose config

    ${bred}dl                 ${normal} = docker logs \$1\n\n
${bold}### functions ###\n
    ${bred}db                 ${normal} = docker build -t \$1 .
    ${bred}drd                ${normal} = docker run -p \$1 -d \$2
    ${bred}de                 ${normal} = winpty docker exec -it \$1 sh
    ${bred}dcs                ${normal} = docker container stop \$1 
    ${bred}dirm               ${normal} = docker image rm -f \$1
    ${bred}dcrm               ${normal} = docker rm -f \$1
    ${bred}dlv                ${normal} = docker logs -f --tail 15 \$1\n\n
${bold}### notes ###\n
    ${bred}winpty docker exec ${normal} = go inside container
    ${bred}                   ${normal} = docker stop $(docker ps -q)\n\n"
    
}
